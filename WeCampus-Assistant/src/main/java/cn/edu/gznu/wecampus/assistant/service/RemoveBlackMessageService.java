package cn.edu.gznu.wecampus.assistant.service;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.h3c.imc.acm.acmuserservice.AcmUserServiceStub;
import com.h3c.imc.acm.acmuserservice.AcmUserServiceStub.RemoveBlackList;
import com.h3c.imc.acm.acmuserservice.AcmUserServiceStub.RemoveBlackListResponse;
import com.h3c.imc.imcplatservice.ImcplatServiceStub;
import com.h3c.imc.imcplatservice.ImcplatServiceStub.Login;
import com.h3c.imc.imcplatservice.ImcplatServiceStub.LoginResponse;

import cn.edu.gznu.wecampus.core.weixin.service.message.ITextMessageService;
import cn.edu.gznu.wecampus.core.weixin.service.message.MessageException;
import cn.edu.gznu.wecampus.core.weixin.service.message.Request;
import cn.edu.gznu.wecampus.core.weixin.service.message.Response;

@Service
public class RemoveBlackMessageService implements ITextMessageService {
	
	@Autowired
	private ImcplatServiceStub imcplatServiceStub;
	
	@Autowired
	private AcmUserServiceStub acmUserServiceStub;
	
	private ServiceClient sc;
	
	public RemoveBlackMessageService() throws AxisFault {
		ConfigurationContext ctx = ConfigurationContextFactory
			.createConfigurationContextFromFileSystem(null, null);
		this.sc = new ServiceClient(ctx, null);
	}

	@Override
	public Response process(Request request) {
		String xgh = request.getFromUserName();
		this.imcRemoveBlack(xgh);
		Response response = new Response();
        response.setToUserName(request.getFromUserName());
        response.setFromUserName(request.getToUserName());
        response.setCreateTime(request.getCreateTime());
        response.setMsgType("text");
        response.setContent("已解除黑名单！");
        return response;
	}

	@Override
	public boolean checkContent(Request request) {
		String content = StringUtils.trim(request.getContent());
		String serviceName = StringUtils.substring(content, 0, 5);
		if(!StringUtils.equals(serviceName, "解除黑名单")) {
			return false;
		}
		return true;
	}
	
	private void imcLogin() {
		try {
			Options opts = new Options();
			opts.setTo(new EndpointReference("http://172.19.200.10:8080/imcws/services/imcplatService"));
			opts.setAction("urn:login");
			opts.setManageSession(true);
			sc.setOptions(opts);
	
			Login login = new Login();
			login.setParam0("ct");
			login.setParam1("Tao598584");
			OMElement element = imcplatServiceStub.toOM(login, true);
	
			OMElement response = sc.sendReceive(element);
			ImcplatServiceStub.WSCommonResult result = LoginResponse.Factory.parse(
				response.getXMLStreamReaderWithoutCaching()
			).get_return();
			if(result.getErrorCode() != 0) {
				throw new MessageException("黑名单解除失败");
			}
		} catch(AxisFault e) {
			
		} catch(Exception e) {
			
		}
	}
	
	private void imcRemoveBlack(String xgh) {
		try {
			AcmUserServiceStub.WSCommonResult result = this.remove(xgh);
			if(result.getErrorCode() == 62300) {
				this.imcLogin();
				result = this.remove(xgh);
			} 
			if(result.getErrorCode() != 0 && result.getErrorCode() != 62327) {
				throw new MessageException("黑名单解除失败");
			}
		} catch(AxisFault e) {
			
		} catch(Exception e) {
			
		}
	}
	
	public AcmUserServiceStub.WSCommonResult remove(String xgh) throws AxisFault, Exception {
		Options opts = new Options();
		opts.setTo(new EndpointReference("http://172.19.200.10:8080/imcws/services/acmUserService"));
		opts.setAction("urn:removeBlackList");
		opts.setManageSession(true);
		sc.setOptions(opts);

		RemoveBlackList removeBlackList = new RemoveBlackList();
		removeBlackList.setParam0(xgh);
		OMElement element = acmUserServiceStub.toOM(removeBlackList, true);
		
		OMElement response = sc.sendReceive(element);
		AcmUserServiceStub.WSCommonResult result = RemoveBlackListResponse.Factory.parse(
			response.getXMLStreamReaderWithoutCaching()).get_return();
		return result;
	}
}
