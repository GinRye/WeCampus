package cn.edu.gznu.wecampus.assistant.config;

import org.apache.axis2.AxisFault;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.h3c.imc.acm.acmuserservice.AcmUserServiceStub;
import com.h3c.imc.imcplatservice.ImcplatServiceStub;

@Configuration
public class ImcConfig {
	@Bean
	public ImcplatServiceStub imcplatServiceStub() throws AxisFault {
		return new ImcplatServiceStub();
	}
	@Bean
	public AcmUserServiceStub acmUserServiceStub() throws AxisFault {
		return new AcmUserServiceStub();
	}
}
