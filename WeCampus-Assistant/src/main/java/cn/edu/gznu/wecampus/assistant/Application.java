package cn.edu.gznu.wecampus.assistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import org.springframework.context.annotation.ComponentScan.Filter;

@SpringBootApplication
@ComponentScan(basePackages = {
	"cn.edu.gznu.wisedu",
	"cn.edu.gznu.wecampus.core",
	"cn.edu.gznu.wecampus.assistant"
}, excludeFilters = {
	@Filter(type = FilterType.ASSIGNABLE_TYPE, value = cn.edu.gznu.wecampus.core.auth.FormApi.class),
	@Filter(type = FilterType.ASSIGNABLE_TYPE, value = cn.edu.gznu.wecampus.core.auth.CasApi.class)
})
public class Application {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
