package cn.edu.gznu.wecampus.assistant.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;

@Configuration
public class JacksonConfig {
	@Bean
	MappingJackson2XmlHttpMessageConverter mappingJackson2XmlHttpMessageConverter() {
		Jackson2ObjectMapperBuilder builder = Jackson2ObjectMapperBuilder.xml();
	    builder.indentOutput(true);
	    return new MappingJackson2XmlHttpMessageConverter(builder.build());
	}
}
