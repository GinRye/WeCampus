package cn.edu.gznu.wecampus.yqfkmj.framework.service;

import cn.edu.gznu.wecampus.core.BusinessException;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.Domitory;

public interface IMemberDomitoryValidator {
	
	boolean isSuitable(String xgh, Domitory domitory);
	
	void validate(String xgh, Domitory domitory) throws BusinessException;
}
