package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_batch")
@Getter
@Setter
public class Batch extends JpaEntity<String> {

	@Column(name = "name")
	private String name;
	
	@Column(name = "start_date")
	private Date startDate;
	
	@Column(name = "end_date")
	private Date endDate = DateUtils.future();
}
