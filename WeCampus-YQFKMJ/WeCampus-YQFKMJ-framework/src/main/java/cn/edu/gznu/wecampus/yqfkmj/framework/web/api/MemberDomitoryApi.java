package cn.edu.gznu.wecampus.yqfkmj.framework.web.api;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.web.FormVo;
import cn.edu.gznu.wecampus.core.web.IAddRest;
import cn.edu.gznu.wecampus.core.web.IEditRest;
import cn.edu.gznu.wecampus.core.web.ISearch;
import cn.edu.gznu.wecampus.core.web.RestResponse;
import cn.edu.gznu.wecampus.core.web.RestResponseElement;
import cn.edu.gznu.wecampus.core.web.SearchVo;
import cn.edu.gznu.wecampus.core.web.annotation.SearchParam;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.MemberDomitory;
import cn.edu.gznu.wecampus.yqfkmj.framework.repository.MemberDomitoryRepository;
import cn.edu.gznu.wecampus.yqfkmj.framework.service.ValidatorService;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/userDomitory")
public class MemberDomitoryApi implements
	ISearch<MemberDomitory, String, MemberDomitoryApi.UserDomitorySearchVo>,
	IAddRest<MemberDomitory, String, MemberDomitoryApi.UserDomitoryFormVo>,
	IEditRest<MemberDomitory, String, MemberDomitoryApi.UserDomitoryFormVo> {
	
	@Autowired
	private MemberDomitoryRepository memberDomitoryRepository;
	
	@Autowired
	private ValidatorService checkValidService;

	@Getter
	@Setter
	public static class UserDomitoryFormVo extends FormVo<String> {
		@NotEmpty(message = "学工号不能为空")
		private String xgh;
		@NotEmpty(message = "宿舍不能为空")
		private String domitoryId;
	}
	
	@Getter
	@Setter
	public static class UserDomitorySearchVo extends SearchVo<MemberDomitory, String> {
		@SearchParam(fields = {"xgh"}, operator = "=")
		private String xgh;
		@SearchParam(fields = {"domitoryId"}, operator = "=")
		private String domitoryId;
	}

	@Override
	public IRepository<MemberDomitory, String> getRepository() {
		return memberDomitoryRepository;
	}

	@Override
	public void processAddEntity(@Valid UserDomitoryFormVo form, MemberDomitory entity) {
		
	}

	@Override
	public void processEditEntity(@Valid UserDomitoryFormVo form, MemberDomitory entity) {
		
	}
	
	@PostMapping("check")
	public RestResponse check(@RequestParam("xgh") String xgh, @RequestParam("domitoryId") String domitoryId) {
		checkValidService.check(xgh, domitoryId);
		RestResponseElement response = new RestResponseElement();
		response.setElement(true);
		return response;
	}
}
