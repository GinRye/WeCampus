package cn.edu.gznu.wecampus.yqfkmj.framework.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.MemberDomitory;

@Repository
public interface MemberDomitoryRepository extends IRepository<MemberDomitory, String> {

	List<MemberDomitory> findByXgh(String xgh);
}
