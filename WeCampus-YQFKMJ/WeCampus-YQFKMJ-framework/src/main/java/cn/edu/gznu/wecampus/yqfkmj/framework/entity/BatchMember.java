package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_batch_member")
@Getter
@Setter
public class BatchMember extends JpaEntity<String> {
	
	@ManyToOne
	@JoinColumn(name = "batch_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_batch_member_batch_id"))
	private Batch batch;
	
	@Column(name = "batch_id", updatable = false, insertable = false)
	private String batchId;

	@Column(name = "xgh", nullable = false)
	private String xgh;
}
