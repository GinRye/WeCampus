package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_validate_log")
@Getter
@Setter
public class ValidateLog extends JpaEntity<String> {
	
	@Column(name = "xgh", nullable = false)
	private String xgh;
	
	@Column(name = "domitory_id")
	private String domitoryId;
	
	@Column(name = "success")
	private Boolean success;
	
	@Column(name = "msg", length = 1024)
	private String msg;
	
}