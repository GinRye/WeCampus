package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_member_domitory")
@Getter
@Setter
public class MemberDomitory extends JpaEntity<String> {
	
	@Column(name = "xgh", nullable = false)
	private String xgh;

	@ManyToOne
	@JoinColumn(name = "domitory_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_member_domitory_domitory_id"))
	private Domitory domitory;
	
	@Column(name = "domitory_id", updatable = false, insertable = false)
	private String domitoryId;
}
