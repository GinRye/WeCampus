package cn.edu.gznu.wecampus.yqfkmj.framework.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.BatchMember;

@Repository
public interface BatchMemberRepository extends IRepository<BatchMember, String> {
	List<BatchMember> findByXgh(String xgh);
}
