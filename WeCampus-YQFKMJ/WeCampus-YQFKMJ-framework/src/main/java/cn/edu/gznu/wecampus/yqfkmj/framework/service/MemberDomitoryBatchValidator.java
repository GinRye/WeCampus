package cn.edu.gznu.wecampus.yqfkmj.framework.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.gznu.wecampus.core.BusinessException;
import cn.edu.gznu.wecampus.core.utils.DateUtils;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.BatchMember;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.Domitory;
import cn.edu.gznu.wecampus.yqfkmj.framework.repository.BatchMemberRepository;
import cn.edu.gznu.wisedu.entity.ldap.Member;
import cn.edu.gznu.wisedu.repository.ldap.MemberRepository;

@Service
public class MemberDomitoryBatchValidator implements IMemberDomitoryValidator {
	
	@Autowired
	private MemberRepository memberRepository;

	@Autowired
	private BatchMemberRepository batchMemberRepository;
	
	@Override
	public void validate(String xgh, Domitory domitory) throws BusinessException {
		List<BatchMember> batchMembers = batchMemberRepository.findByXgh(xgh);
		for(BatchMember batchMember : batchMembers) {
			Date startDate = batchMember.getBatch().getStartDate();
			Date endDate = batchMember.getBatch().getEndDate() == null ? DateUtils.future() : batchMember.getBatch().getEndDate();
			Date today = DateUtils.today();
			if(DateUtils.compareDateByDay(today, startDate) >= 0 && DateUtils.compareDateByDay(today, endDate) <= 0) {
				return;
			}
		}
		throw new BusinessException("不在通行时间内");
	}

	@Override
	public boolean isSuitable(String xgh, Domitory domitory) {
		/*
		Member member = memberRepository.findByXgh(xgh);
		if(member.getType() == Member.Type.本科生 || member.getType() == Member.Type.研究生) {
			return true;
		}
		*/
		return false;
	}
}
