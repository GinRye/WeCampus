package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_domitory")
@Getter
@Setter
public class Domitory extends JpaEntity<String> {
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "school_district")
	@Enumerated(EnumType.STRING)
	private SchoolDistrict schoolDistrict;
	
	@Column(name = "is_gate")
	private boolean isGate;
	
	@OneToMany
	@JoinColumn(name = "domitory_id")
	@JsonIgnore
	private List<MemberDomitory> memberDomitories = new LinkedList<MemberDomitory>();
}
