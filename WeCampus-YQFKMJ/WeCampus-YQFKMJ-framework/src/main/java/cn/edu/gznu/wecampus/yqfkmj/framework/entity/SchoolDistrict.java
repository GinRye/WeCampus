package cn.edu.gznu.wecampus.yqfkmj.framework.entity;

import lombok.Getter;

public enum SchoolDistrict {
	全校区("全校区"),
	宝山校区("宝山校区"),
	花溪校区("花溪校区"),
	白云校区("白云校区");
	
	@Getter
	private String label;
	
	private SchoolDistrict(String label) {
		this.label = label;
	}
}
