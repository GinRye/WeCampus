package cn.edu.gznu.wecampus.yqfkmj.framework.web.api;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.web.ISearch;
import cn.edu.gznu.wecampus.core.web.SearchVo;
import cn.edu.gznu.wecampus.core.web.annotation.SearchParam;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.ValidateLog;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/validateLog")
public class ValidateLogApi implements
	ISearch<ValidateLog, String, ValidateLogApi.ValidateLogSearchVo> {
	
	@Getter
	@Setter
	public static class ValidateLogSearchVo extends SearchVo<ValidateLog, String> {
		@SearchParam(fields = {"success"}, operator = "=")
		private Boolean success;
		@SearchParam(fields = {"domitoryId"}, operator = "=")
		private String domitoryId;
		@SearchParam(fields = {"createTime"}, operator = ">=")
		@DateTimeFormat(pattern = "yyyy-MM-dd")
		private Date startTime;
		@SearchParam(fields = {"createTime"}, operator = "<=")
		@DateTimeFormat(pattern = "yyyy-MM-dd")
		private Date endTime;
	}
}
