package cn.edu.gznu.wecampus.yqfkmj.framework.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.gznu.wecampus.core.BusinessException;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.Domitory;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.MemberDomitory;
import cn.edu.gznu.wecampus.yqfkmj.framework.repository.MemberDomitoryRepository;

@Service
public class MemberDomitoryRelationValidator implements IMemberDomitoryValidator {

	@Autowired
	private MemberDomitoryRepository memberDomitoryRepository;
	
	@Override
	public void validate(String xgh, Domitory domitory) throws BusinessException {
		List<MemberDomitory> memberDomitories = memberDomitoryRepository.findByXgh(xgh);
		for(MemberDomitory memberDomitory : memberDomitories) {
			if(StringUtils.equals(memberDomitory.getDomitoryId(), domitory.getId())) {
				return;
			}
		}
		throw new BusinessException("未授予通行权限");
	}

	@Override
	public boolean isSuitable(String xgh, Domitory domitory) {
		return true;
	}
}
