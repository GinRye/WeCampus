package cn.edu.gznu.wecampus.yqfkmj.framework.repository;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.yqfkmj.framework.entity.ValidateLog;

@Repository
public interface ValidateLogRepository extends IRepository<ValidateLog, String> {

}
