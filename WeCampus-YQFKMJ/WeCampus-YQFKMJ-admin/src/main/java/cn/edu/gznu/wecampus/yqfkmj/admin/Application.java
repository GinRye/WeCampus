package cn.edu.gznu.wecampus.yqfkmj.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages = {
	"cn.edu.gznu.wisedu",
	"cn.edu.gznu.wecampus.core",
	"cn.edu.gznu.wecampus.yqfkmj"
}, excludeFilters = {
	@Filter(type = FilterType.ASSIGNABLE_TYPE, value = cn.edu.gznu.wecampus.core.auth.WeixinApi.class),
	@Filter(type = FilterType.REGEX, pattern = "cn.edu.gznu.wecampus.core.weixin.*")
})
public class Application {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
