export default class Permission {
	constructor({rid, label}) {
		this.rid = rid;
		this.label = label;
	}
}