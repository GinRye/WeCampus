import Permission from './Permission.js';

export default class Page extends Permission {
	constructor({rid, label, icon, isOpen, children, actions}) {
		super({rid, label});
		this.icon = icon;
		this.isOpen = isOpen;
		this.children = children;
		this.actions = actions;
	}
}