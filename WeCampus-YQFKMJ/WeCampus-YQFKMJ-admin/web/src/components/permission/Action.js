import Permission from './Permission.js';
import request from '@/utils/request';

export default class Action extends Permission {
	constructor({rid, label, method, url}) {
		super({rid, label});
		this.method = method;
		this.url = url;
	}
	doAction(config) {
		config = config || {};
		config.url = this.url;
		config.method = this.method;
		return request(config);
	}
}