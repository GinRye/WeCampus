import {getStore, setStore, clearStore} from '@/utils/storage'

export const userKey = 'user'

export function getUser() {
	var user = getStore(userKey);
	if(!user) {
		clearUser();
		return null;
	}
	return user;
}

export function setUser(user) {
	return setStore(userKey, user)
}

export function clearUser() {
	return clearStore(userKey)
}
