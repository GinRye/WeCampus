import Vue from 'vue';
import Router from 'vue-router';

import Layout from '@/views/layout';
import store from '@/store';
import routes from './routes.js';
import request from '@/utils/request.js';

Vue.use(Router);

var router = new Router({
	routes: routes
});

router.beforeEach((to, from, next) => {
	request({
		url: 'api/userInfo/get',
		method: 'get'
	}).then(data => {
		store.state.global.userPermissionIds = data.element.permissionIds;
		if(to.path == '/login') {
			next('/dashboard');
		} else {
			next();
		}
	}).catch(err => {
		if(err.detail.code == 10 && to.path != '/login') {
			next('/login');
		} else {
			next();
		}
	});
});

export default router;