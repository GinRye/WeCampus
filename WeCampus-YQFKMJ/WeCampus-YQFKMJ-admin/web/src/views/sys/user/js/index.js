import Vue from 'vue';
import Vuex from 'vuex';
import request from '@/utils/request';

let appStore = new Vuex.Store({
	modules: {
		app: {
			state: {
				list: [],
				totalElements: 0,
				totalPages: 0,
				searchParams: {
					size: 20,
					page: 1
				}
			}
		},
		config: {
			state: {
				roles: [],
				roleMap: {}
			}
		}
	}
});

let eventBus = new Vue({});

request({
	url: 'api/common/role/search',
	params: { page: 1, size: 10000 }
}).then(data => {
	appStore.state.config.roles = data.list;
	data.list.forEach(role => {
		appStore.state.config.roleMap[role.id] = role;
	});
}).catch(error => {
	console.error(error);
});

export default { appStore, eventBus };