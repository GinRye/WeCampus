import Vue from 'vue';
import Vuex from 'vuex';

let appStore = new Vuex.Store({
	modules: {
		app: {
			state: {
				list: [],
				totalElements: 0,
				totalPages: 0,
				searchParams: {
					size: 20,
					page: 1
				}
			}
		}
	}
});

let eventBus = new Vue({});

export default { appStore, eventBus };