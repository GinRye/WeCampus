<template>
	<div>
		<search @search="search"></search>
		<xtable></xtable>
		<el-pagination :current-page="$store.state.app.searchParams.page" :page-size="$store.state.app.searchParams.size"
			:page-sizes="[10, 20, 50, 100, 1000]" :page-count="$store.state.app.totalPages" :total="$store.state.app.totalElements"
			@current-change="$store.state.app.searchParams.page = $event" 
			@size-change="$store.state.app.searchParams.size = $event"
			layout="total, sizes, prev, pager, next, jumper">
		</el-pagination>
	</div>
</template>

<script>
import cfg from './js/';
import xtable from './xtable';
import search from './search';
import store from '@/store';
import request from '@/utils/request';

export default {
	store: cfg.appStore,
	components: {
		'xtable': xtable,
		'search': search
	},
	data() {
		return {}
	},
	methods: {
		search() {
			let self = this;
			let loading = self.$loading({
				target: self.$el,
				lock: true,
				text: '查询用户中...',
				spinner: 'el-icon-loading',
				background: 'rgba(0, 0, 0, 0.7)'
			});
			let config = {};
			config.params = self.$store.state.app.searchParams;
			let action = store.state.global.permissionMap['access-control:log:search'];
			action.doAction(config).then(data => {
				self.$store.state.app.list = data.list;
				self.$store.state.app.totalPages = data.totalPages;
				self.$store.state.app.totalElements = data.totalElements;
				loading.close();
			}).catch(error => {
				self.$message({
					message: '查询用户失败：' + error.detail.msg,
					type: 'error',
					showClose: true
				});
				loading.close();
			});
		},
		submit({method, formData}) {
			this[method](formData);
		}
	},
	watch: {
		'$store.state.app.searchParams.page': function(newVal, oldVal) {
			this.search();
		},
		'$store.state.app.searchParams.size': function(newVal, oldVal) {
			this.search();
		}
	},
	mounted() {
		this.search();
	}
}
</script>