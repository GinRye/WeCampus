import Vue from 'vue';
import Element from 'element-ui';

import 'element-ui/lib/theme-chalk/index.css';
import 'font-awesome/css/font-awesome.css'

import App from '@/App.vue';
import router from '@/router/index.js';
import store from '@/store/index.js';
import permission from '@/directive/permission/index.js' // 权限判断指令

Vue.config.productionTip = false;

Vue.use(Element);
Vue.directive('permission', permission);

new Vue({
	render: h => h(App),
	router,
	store
}).$mount('#app');

