import Permission from '@/components/permission/Permission.js';
import Page from '@/components/permission/Page.js';
import Action from '@/components/permission/Action.js';

let permissions = [
	new Page({
		rid: 'sys', label: '系统管理', icon: 'fa fa-cog fa-fw', isOpen: false,
		children: [
			new Page({
				rid: 'user', label: '用户管理', icon: 'fa fa-user fa-fw', isOpen: true,
				actions: [
					new Action({rid: 'search', label: '查询用户', method: 'GET', url: 'api/common/user/search'}),
					new Action({rid: 'add', label: '添加用户', method: 'POST', url: 'api/common/user/add'}),
					new Action({rid: 'edit', label: '编辑用户', method: 'POST', url: 'api/common/user/edit'}), 
					new Action({rid: 'remove', label: '删除用户', method: 'POST', url: 'api/common/user/remove'}), 
					new Action({rid: 'assignRoles', label: '分配角色', method: 'POST', url: 'api/common/user/assignRoles'})
				]
			}),
			new Page({
				rid: 'role', label: '角色管理', icon: 'fa fa-users fa-fw', isOpen: true,
				actions: [
					new Action({rid: 'search', label: '查询角色', method: 'GET', url: 'api/common/role/search'}), 
					new Action({rid: 'add', label: '添加角色', method: 'POST', url: 'api/common/role/add'}),
					new Action({rid: 'edit', label: '编辑角色', method: 'POST', url: 'api/common/role/edit'}),
					new Action({rid: 'remove', label: '删除角色', method: 'POST', url: 'api/common/role/remove'}),
					new Action({rid: 'assignPermissions', label: '分配权限', method: 'POST', url: 'api/common/role/assignPermissions'})
				],
				children: [
					new Page({
						rid: 'group', label: '角色分组', icon: 'fa fa-user-circle fa-fw', isOpen: true,
						actions: [
							new Action({rid: 'search', label: '查询分组', method: 'POST', url: 'api/common/role/group/search'}), 
							new Action({rid: 'add', label: '添加分组', method: 'POST', url: 'api/common/role/group/add'}), 
							new Action({rid: 'edit', label: '编辑分组', method: 'POST', url: 'api/common/role/group/edit'}), 
							new Action({rid: 'remove', label: '删除分组', method: 'POST', url: 'api/common/role/group/remove'})
						]
					})
				]
			})
		]
	}),
	new Page({
		rid: 'access-control', label: '门禁管理', icon: 'fa fa-credit-card fa-fw', isOpen: false,
		children: [
			new Page({
				rid: 'door', label: '楼栋管理', icon: 'fa fa-university fa-fw', isOpen: true,
				actions: [
					new Action({rid: 'search', label: '查询楼栋', method: 'GET', url: 'api/domitory/search'}), 
					new Action({rid: 'add', label: '添加楼栋', method: 'POST', url: 'api/domitory/add'}),
					new Action({rid: 'edit', label: '编辑楼栋', method: 'POST', url: 'api/domitory/edit'}),
					new Action({rid: 'remove', label: '删除楼栋', method: 'POST', url: 'api/domitory/remove'}),
					new Action({rid: 'authorize', label: '楼栋授权', method: 'POST', url: 'api/domitory/authorize'})
				]
			}),
			new Page({
				rid: 'batch', label: '批次管理', icon: 'fa fa-sign-in fa-fw', isOpen: true,
				actions: [
					new Action({rid: 'search', label: '查询批次', method: 'GET', url: 'api/batch/search'}), 
					new Action({rid: 'add', label: '添加批次', method: 'POST', url: 'api/batch/add'}),
					new Action({rid: 'edit', label: '编辑批次', method: 'POST', url: 'api/batch/edit'}),
					new Action({rid: 'remove', label: '删除批次', method: 'POST', url: 'api/batch/remove'}),
					new Action({rid: 'authorize', label: '批次授权', method: 'POST', url: 'api/batch/authorize'})
				]
			}),
			new Page({
				rid: 'log', label: '出入日志管理', icon: 'fa fa-file fa-fw', isOpen: true,
				actions: [
					new Action({rid: 'search', label: '查询日志', method: 'GET', url: 'api/validateLog/search'})
				]
			})
		]
	})
];
let permissionMap = {};

let queue = [];
let i = 0;
while(i < permissions.length || queue.length > 0) {
	if(queue.length == 0) {
		queue.push(permissions[i++]);
		continue;
	}
	let permission = queue.shift();
	permission.id = permission.pid == null ? permission.rid : (permission.pid + ":" + permission.rid);
	if(permission.actions && permission.actions.length > 0) {
		permission.actions.forEach(button => {
			button.id = permission.id + ":" + button.rid;
			button.parent = permission;
			permissionMap[button.id] = button;
		});
	}
	if(permission.children && permission.children.length > 0) {
		permission.children.forEach(child => {
			child.pid = permission.id;
			child.parent = permission;
			queue.push(child);
		});
	}
	permissionMap[permission.id] = permission;
}

export default { permissions, permissionMap };