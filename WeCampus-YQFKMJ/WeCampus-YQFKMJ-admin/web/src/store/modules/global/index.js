import perm from './permission.js';

const global = {
	state: {
		device: 'desktop',
		permissions: perm.permissions,
		permissionMap: perm.permissionMap,
		userPermissionIds: []
	}
}

export default global;