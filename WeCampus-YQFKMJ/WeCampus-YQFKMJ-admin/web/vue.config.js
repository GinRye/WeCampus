module.exports = {
	publicPath: './',
	outputDir: './dist',
	assetsDir: 'assets',
    runtimeCompiler: true,
	pages: {
		index: {
			entry: 'src/main.js',
			template: 'src/index.html',
			filename: 'index.html',
			favicon: './src/assets/logo.png'
		}
	},
	configureWebpack: config => {
		if(config.mode != 'production') {
			config.devtool = 'inline-source-map';
		} else {
			config.devtool = '(none)';
		}
	},
	devServer: {
		port: 8112,
		disableHostCheck: true,
		compress: true
	}
}

