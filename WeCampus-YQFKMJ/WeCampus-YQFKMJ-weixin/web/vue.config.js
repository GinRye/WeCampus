module.exports = {
	publicPath: './',
	outputDir: './dist',
	assetsDir: 'assets',
    runtimeCompiler: true,
	pages: {
		index: {
			entry: 'src/main.js',
			template: 'src/index.html',
			filename: 'index.html'
		}
	},
	configureWebpack: config => {
		if(config.mode != 'production') {
			config.devtool = 'inline-source-map';
		} else {
			config.devtool = '(none)';
		}
	},
	devServer: {
		port: 8102,
		disableHostCheck: true
	}
}

