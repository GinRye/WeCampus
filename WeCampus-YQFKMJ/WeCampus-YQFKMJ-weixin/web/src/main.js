import 'framework7/css/framework7.bundle.css';
import 'framework7-icons/css/framework7-icons.css';

import Vue from 'vue';
import Framework7 from 'framework7/js/framework7.bundle.js';
import Framework7Vue from 'framework7-vue/framework7-vue.bundle.js';

import App from '@/App.vue';

Framework7.use(Framework7Vue);

new Vue({
  render: h => h(App),
}).$mount('#app')
