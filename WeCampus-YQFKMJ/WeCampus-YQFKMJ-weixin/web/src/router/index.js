import auth from '@/views/auth.vue';
import index from '@/views/index.vue';

export default [{
	path: '/auth',
	component: auth,
	name: 'auth'
}, {
	path: '/',
	component: index,
	name: 'index'
}];
