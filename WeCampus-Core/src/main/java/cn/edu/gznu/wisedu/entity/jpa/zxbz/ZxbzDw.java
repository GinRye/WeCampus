package cn.edu.gznu.wisedu.entity.jpa.zxbz;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wisedu.entity.jpa.JpaWiseduEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "usr_zxbz", name = "t_zxbz_dw")
@Getter
@Setter
public class ZxbzDw extends JpaWiseduEntity<String> implements Serializable {

	private static final long serialVersionUID = 2464362656152469667L;

	@Column(name = "dm")
	private String dm;

	@Column(name = "mc")
	private String mc;
}
