package cn.edu.gznu.wisedu.repository.jpa.zxbz;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wisedu.entity.jpa.zxbz.ZxbzDw;

@Repository
public interface ZxbzDwRepository extends IRepository<ZxbzDw, String> {
	ZxbzDw findByDm(String dm);
}
