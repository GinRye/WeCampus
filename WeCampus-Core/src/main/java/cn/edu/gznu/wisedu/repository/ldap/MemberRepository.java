package cn.edu.gznu.wisedu.repository.ldap;

import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.query.SearchScope;
import org.springframework.stereotype.Repository;

import cn.edu.gznu.wisedu.entity.ldap.Member;

@Repository
public interface MemberRepository extends LdapRepository<Member> {

	default Member findByXgh(String xgh) {
		LdapQuery query = LdapQueryBuilder.query()
			.base("ou=People")
			.searchScope(SearchScope.SUBTREE)
			.where("uid")
			.is(xgh);
		return this.findOne(query).orElse(null);
	}
}
