package cn.edu.gznu.wisedu.api.jpa.zsj;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.web.ISearch;
import cn.edu.gznu.wisedu.api.jpa.PersonApi;
import cn.edu.gznu.wisedu.api.jpa.PersonApi.PersonSearchVo;
import cn.edu.gznu.wisedu.entity.jpa.zsj.Yjs;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/wisedu/jpa/zsj/yjs")
public class YjsApi implements
	ISearch<Yjs, String, YjsApi.YjsSearchVo>,
	PersonApi<Yjs> {

	@Getter
	@Setter
	public static class YjsSearchVo extends PersonSearchVo<Yjs> {}
}
