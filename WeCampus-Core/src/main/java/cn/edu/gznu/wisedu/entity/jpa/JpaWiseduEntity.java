package cn.edu.gznu.wisedu.entity.jpa;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import cn.edu.gznu.wecampus.core.JpaEntity;

@MappedSuperclass
@AttributeOverrides({
	@AttributeOverride(name = "id", column = @Column(name = "wid")),
	@AttributeOverride(name = "createTime", column = @Column(name = "clrq", insertable = false, updatable = false)),
	@AttributeOverride(name = "createUser", column = @Column(name = "czlx", insertable = false, updatable = false)),
	@AttributeOverride(name = "updateTime", column = @Column(name = "clrq", insertable = false, updatable = false)),
	@AttributeOverride(name = "updateUser", column = @Column(name = "czlx", insertable = false, updatable = false))
})
public abstract class JpaWiseduEntity<ID extends Serializable> extends JpaEntity<ID> {

}
