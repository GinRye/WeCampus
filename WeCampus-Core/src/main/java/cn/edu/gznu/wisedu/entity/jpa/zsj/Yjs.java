package cn.edu.gznu.wisedu.entity.jpa.zsj;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wisedu.entity.jpa.Person;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "usr_zsj", name = "t_yjs")
@Getter
@Setter
public class Yjs extends Person {

}
