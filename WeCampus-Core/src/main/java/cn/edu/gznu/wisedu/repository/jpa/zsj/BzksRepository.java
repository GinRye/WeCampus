package cn.edu.gznu.wisedu.repository.jpa.zsj;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wisedu.entity.jpa.zsj.Bzks;
import cn.edu.gznu.wisedu.repository.jpa.PersonRepository;

@Repository
public interface BzksRepository extends IRepository<Bzks, String>, PersonRepository<Bzks> {
	
}
