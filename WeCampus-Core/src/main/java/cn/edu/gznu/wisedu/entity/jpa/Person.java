package cn.edu.gznu.wisedu.entity.jpa;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import cn.edu.gznu.wisedu.entity.jpa.zxbz.ZxbzDw;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class Person extends JpaWiseduEntity<String> {

	@Column(name = "xh")
	private String xgh;
	
	@Column(name = "xm")
	private String xm;
	
	@Column(name = "sfzjh")
	private String sfzjh;

	@ManyToOne
	@JoinColumn(name = "dwdm", referencedColumnName = "dm")
	private ZxbzDw dw;

	@Column(name = "dwdm", insertable = false, updatable = false)
	private String dwdm;
}
