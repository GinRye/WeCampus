package cn.edu.gznu.wisedu;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;

@Configuration
@EnableLdapRepositories
public class WiseduLdapConfig {

}
