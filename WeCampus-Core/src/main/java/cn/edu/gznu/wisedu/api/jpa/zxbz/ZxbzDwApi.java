package cn.edu.gznu.wisedu.api.jpa.zxbz;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.web.ISearch;
import cn.edu.gznu.wecampus.core.web.SearchVo;
import cn.edu.gznu.wecampus.core.web.annotation.SearchParam;
import cn.edu.gznu.wisedu.entity.jpa.zxbz.ZxbzDw;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/wisedu/jpa/zxbz/zxbzDw")
public class ZxbzDwApi implements
	ISearch<ZxbzDw, String, ZxbzDwApi.ZxbzDwSearchVo> {

	@Getter
	@Setter
	public static class ZxbzDwSearchVo extends SearchVo<ZxbzDw, String> {
		@SearchParam(fields = "dm", operator = "like")
		private String dm;
		@SearchParam(fields = "dm", operator = "in")
		private List<String> dms;
	}
}
