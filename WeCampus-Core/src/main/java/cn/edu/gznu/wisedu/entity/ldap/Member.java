package cn.edu.gznu.wisedu.entity.ldap;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Name;

import org.apache.commons.lang3.StringUtils;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Entry(objectClasses = {"top", "person"})
@Getter
@Setter
public class Member {

	@JsonIgnore
	@Id
	private Name id;
	
	@DnAttribute(value = "uid")
	@JsonProperty("xgh")
	private String uid;
	
	@Attribute(name = "cn")
	@JsonProperty("xm")
	private String commonName;
	
	@Attribute(name = "eduPersonCardID")
	@JsonProperty("sfzjh")
	private String eduPersonCardID;
	
	@Attribute(name = "eduPersonOrgDN")
	@JsonProperty("dwdm")
	private String eduPersonOrgDN;

	@JsonIgnore
	@Attribute(name = "userPassword")
	private String userPassword;

	@JsonIgnore
	@Attribute(name = "otherPassword")
	private String otherPassword;
	
	@JsonProperty(value = "type")
	public Type getType() {
		String unparsed = this.id.toString();
		String[] splits = unparsed.split(",");
		for(String split : splits) {
			String[] pairs = split.split("=");
			if(StringUtils.equals(pairs[0], "ou")) {
				String code = pairs[1];
				Type type = Type.convert(code);
				if(type != null) {
					return type;
				}
			}
		}
		return Type.其他;
	}
	
	public static enum Type {
		教职工("jzg"),
		本科生("bzks"),
		研究生("yjs"),
		其他("");
		
		@Getter
		private String code;
		private static Map<String, Type> map = new HashMap<String, Type>();
		static {
			map.put(Type.教职工.code, Type.教职工);
			map.put(Type.本科生.code, Type.本科生);
			map.put(Type.研究生.code, Type.研究生);
			map.put(Type.其他.code, Type.其他);
		}
		
		private Type(String code) {
			this.code = code;
		}
		public static Type convert(String code) {
			return map.get(code);
		}
	}
}
