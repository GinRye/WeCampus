package cn.edu.gznu.wisedu.api.jpa.zsj;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.web.ISearch;
import cn.edu.gznu.wisedu.api.jpa.PersonApi;
import cn.edu.gznu.wisedu.api.jpa.PersonApi.PersonSearchVo;
import cn.edu.gznu.wisedu.entity.jpa.zsj.Jzg;
import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api/wisedu/jpa/zsj/jzg")
public class JzgApi implements
	ISearch<Jzg, String, JzgApi.JzgSearchVo>,
	PersonApi<Jzg> {

	@Getter
	@Setter
	public static class JzgSearchVo extends PersonSearchVo<Jzg> {}
}
