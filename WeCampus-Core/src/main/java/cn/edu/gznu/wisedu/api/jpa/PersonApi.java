package cn.edu.gznu.wisedu.api.jpa;

import org.springframework.web.bind.annotation.GetMapping;

import cn.edu.gznu.wecampus.core.web.IRest;
import cn.edu.gznu.wecampus.core.web.RestResponseList;
import cn.edu.gznu.wecampus.core.web.SearchVo;
import cn.edu.gznu.wecampus.core.web.annotation.SearchParam;
import cn.edu.gznu.wisedu.entity.jpa.Person;
import cn.edu.gznu.wisedu.repository.jpa.PersonRepository;
import lombok.Getter;
import lombok.Setter;

public interface PersonApi<T extends Person> extends IRest<T, String> {
	
	@SuppressWarnings("rawtypes")
	@GetMapping("findGroupDwdm")
	default RestResponseList findGroupDwdm() {
		PersonRepository repository = (PersonRepository) this.getRepository();
		RestResponseList response = new RestResponseList();
		response.setList(repository.findGroupDwdm());
		return response;
	}
	
	@Getter
	@Setter
	class PersonSearchVo<T extends Person> extends SearchVo<T, String> {
		@SearchParam(fields = {"xgh", "xm"}, operator = "like")
		private String name;
		@SearchParam(fields = {"dwdm"}, operator = "like")
		private String dwdm;
	}
}
