package cn.edu.gznu.wisedu.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.edu.gznu.wisedu.entity.jpa.Person;

public interface PersonRepository<T extends Person> {
	
	@Query("select o from #{#entityName} o where o.xgh = :xgh")
	T findByXgh(@Param("xgh") String xgh);
	
	@Query("select o.dwdm from #{#entityName} o group by o.dwdm")
	List<String> findGroupDwdm();
}
