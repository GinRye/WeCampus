package cn.edu.gznu.wisedu.entity.jpa.zsj;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import cn.edu.gznu.wisedu.entity.jpa.Person;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "usr_zsj", name = "t_jzg")
@AttributeOverrides({
	@AttributeOverride(name = "xgh", column = @Column(name = "zgh", insertable = false, updatable = false)),
	@AttributeOverride(name = "dwdm", column = @Column(name = "szdwdm", insertable = false, updatable = false))
})
@AssociationOverrides({
	@AssociationOverride(name = "dw", joinColumns = {
		@JoinColumn(name = "szdwdm", referencedColumnName = "dm")
	})
})
@Getter
@Setter
public class Jzg extends Person {

}
