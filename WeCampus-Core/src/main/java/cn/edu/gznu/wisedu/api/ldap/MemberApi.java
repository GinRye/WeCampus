package cn.edu.gznu.wisedu.api.ldap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.gznu.wecampus.core.web.RestResponse;
import cn.edu.gznu.wecampus.core.web.RestResponseElement;
import cn.edu.gznu.wisedu.entity.ldap.Member;
import cn.edu.gznu.wisedu.repository.ldap.MemberRepository;

@RestController
@RequestMapping("/api/wisedu/ldap/member")
public class MemberApi {
	
	@Autowired
	private MemberRepository memberRepository;
	
	@GetMapping("findByXgh")
	public RestResponse findByXgh(String xgh) {
		Member member = memberRepository.findByXgh(xgh);
		RestResponseElement response = new RestResponseElement();
		response.setElement(member);
		return response;
	}
}
