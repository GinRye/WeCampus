package cn.edu.gznu.wisedu.repository.jpa.zsj;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wisedu.entity.jpa.zsj.Jzg;
import cn.edu.gznu.wisedu.repository.jpa.PersonRepository;

@Repository
public interface JzgRepository extends IRepository<Jzg, String>, PersonRepository<Jzg> {
	
}
