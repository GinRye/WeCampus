package cn.edu.gznu.wisedu;

import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@EnableJpaRepositories(
	entityManagerFactoryRef = "wiseduKFPTDBEntityManagerFactory",
	transactionManagerRef = "wiseduKFPTDBTransactionManager",
	basePackages = {"cn.edu.gznu.wisedu.**.repository"}
)
@EnableTransactionManagement(proxyTargetClass = true)
public class WiseduKFPTDBConfig {
	
	@Bean("wiseduKFPTDBDataSource")
	public DataSource dataSource() throws SQLException {
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(
			"jdbc:oracle:thin:@(description=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.0.7)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=172.24.0.8)(PORT=1521)))(FAILOVER=yes)(LOAD_BALANCE=yes)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=KFPTDB)))"
		);
		dataSource.setUsername("usr_zsj_read");
		dataSource.setPassword("zsjRead821");
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setFilters("stat");
		return dataSource;
	}
	
	@Bean("wiseduKFPTDBJdbcTemplate")
	public JdbcTemplate jdbcTemplate(
		@Qualifier("wiseduKFPTDBDataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean("wiseduKFPTDBJpaVendorAdapter")
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(true);
		jpaVendorAdapter.setGenerateDdl(true);
		return jpaVendorAdapter;
	}
	
	@Bean("wiseduKFPTDBEntityManagerFactory")
	@Autowired
	public AbstractEntityManagerFactoryBean entityManagerFactory(
		@Qualifier("wiseduKFPTDBDataSource") DataSource dataSource, 
		@Qualifier("wiseduKFPTDBJpaVendorAdapter") JpaVendorAdapter jpaVendorAdapter) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = 
			new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		entityManagerFactoryBean.setPackagesToScan("cn.edu.gznu.wisedu.**.entity");
		Properties jpaProperties = new Properties();
		jpaProperties.setProperty("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
		jpaProperties.setProperty("hibernate.search.default.directory_provider", "filesystem");
		jpaProperties.setProperty("hibernate.search.default.indexBase", "indexes");
		jpaProperties.setProperty("hibernate.show_sql", "true");
		jpaProperties.setProperty("hibernate.hbm2ddl.auto", "none");
		jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		entityManagerFactoryBean.setJpaProperties(jpaProperties);
		return entityManagerFactoryBean;
	}
	
	@Bean("wiseduKFPTDBTransactionManager")
	@Autowired
	public PlatformTransactionManager transactionManager(
		@Qualifier("wiseduKFPTDBEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
	
}
