package cn.edu.gznu.wecampus.core.weixin.entity;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class WeixinEntity {
	protected Date createTime = new Date();
	protected int errcode;
	protected String errmsg;
}