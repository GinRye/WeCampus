package cn.edu.gznu.wecampus.core.common.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_common_role_permission")
@Getter
@Setter
public class RolePermission extends JpaEntity<String> {

	@ManyToOne
	@JoinColumn(name = "role_id", 
		foreignKey = @ForeignKey(name = "fk_tb_common_role_permission_role_id"))
	private Role role;

	@Column(name = "role_id", updatable = false, insertable = false)
	private String roleId;

	@Column(name = "permission")
	private String permission;
}
