package cn.edu.gznu.wecampus.core.weixin.entity;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WeixinExpiresEntity extends WeixinEntity {

	@JsonProperty("expires_in")
	protected int expiresIn;
	public Date getExpiresTime() {
		return DateUtils.addSeconds(this.getCreateTime(), this.expiresIn);
	}
	public boolean isExpire() {
		Date now = new Date();
		if(now.compareTo(this.getExpiresTime()) > 0) {
			return false;
		} else {
			return true;
		}
	}
}
