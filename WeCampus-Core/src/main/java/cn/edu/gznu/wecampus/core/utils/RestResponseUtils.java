package cn.edu.gznu.wecampus.core.utils;

import cn.edu.gznu.wecampus.core.web.RestResponse;

public class RestResponseUtils {
	
	public static RestResponse ok(String msg) {
		return create(true, 0, msg);
	}
	
	public static RestResponse fail(int code, String msg) {
		return create(false, code, msg);
	}
	
	public static RestResponse create(boolean success, int code, String msg) {
		RestResponse response = new RestResponse();
		response.setSuccess(success);
		response.getDetail().setCode(code);
		response.getDetail().setMsg(msg);
		return response;
	}
}
