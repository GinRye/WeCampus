package cn.edu.gznu.wecampus.core.web.annotation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.springframework.stereotype.Component;

@Component
public class CriteriaLessOperator extends AbstractCriteriaOperator {

	public CriteriaLessOperator() {
		super("<");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Predicate createPredicate(CriteriaBuilder cb, From from, String field, Object value) {
		return cb.lessThan(from.get(field), (Comparable) value);
	}
}
