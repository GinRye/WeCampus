package cn.edu.gznu.wecampus.core.web;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponseList extends RestResponse {
	private List<?> list;
}
