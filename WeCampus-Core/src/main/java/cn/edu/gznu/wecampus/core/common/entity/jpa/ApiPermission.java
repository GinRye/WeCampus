package cn.edu.gznu.wecampus.core.common.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_common_api_permission")
@Getter
@Setter
public class ApiPermission extends JpaEntity<String> {
	
	@Column(name = "api")
	private String api;
	
	@Column(name = "permission")
	private String permission;
}
