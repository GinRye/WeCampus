package cn.edu.gznu.wecampus.core.web;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponseMap extends RestResponse {
	private Map<String, ?> map;
}
