package cn.edu.gznu.wecampus.core.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

public class AuthUtils {
	
	private static final String VALIDATE_CODE_KEY = "VALIDATE_CODE_KEY";
		
	public static String getUserId() {
		Subject subject = SecurityUtils.getSubject();
		String userId = (String) subject.getPrincipal();
		return userId;
	}
	
	public static void putValidateCode(String validateCode) {
		Session session = SecurityUtils.getSubject().getSession();
		session.setAttribute(VALIDATE_CODE_KEY, validateCode);
	}
	
	public static String getValidateCode() {
		Session session = SecurityUtils.getSubject().getSession();
		return (String) session.getAttribute(VALIDATE_CODE_KEY);
	}
}
