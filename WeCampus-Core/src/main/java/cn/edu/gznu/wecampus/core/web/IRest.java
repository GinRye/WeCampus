package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;
import java.util.List;

import org.springframework.core.ResolvableType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.edu.gznu.wecampus.core.ApplicationContextHolder;
import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.JpaEntity;

public interface IRest<T extends JpaEntity<ID>, ID extends Serializable> {
	
	default RestResponseElement createRestResponseElement(T entity) {
		RestResponseElement response = new RestResponseElement();
		response.setSuccess(true);
		response.setElement(entity);
		return response;
	}
	
	default RestResponseList createRestResponseList(List<T> entities) {
		RestResponseList response = new RestResponseList();
		response.setSuccess(true);
		response.setList(entities);
		return response;
	}

	default RestResponsePageList createRestResponsePageList(
			List<T> entities, int page, int size, int totalPages, int totalElements) {
		RestResponsePageList response = new RestResponsePageList();
		response.setList(entities);
		response.setPage(page);
		response.setSize(size);
		response.setTotalPages(totalPages);
		response.setTotalElements(totalElements);
		response.setSuccess(true);
		return response;
	}
	
	default RestResponsePageList createRestResponsePageList(
			Pageable pageable, Page<T> entities) {
		return this.createRestResponsePageList(
			entities.getContent(), pageable.getPageNumber() + 1, pageable.getPageSize(), 
			entities.getTotalPages(), (int) entities.getTotalElements()
		);
	}
	
	default Class<?> getEntityClass() {
		ResolvableType thisResolvableType = ResolvableType.forClass(this.getClass());
		ResolvableType[] interfaceTypes = thisResolvableType.getInterfaces();
		ResolvableType entityResolvableType = null;
		for(ResolvableType interfaceType : interfaceTypes) {
			if(ResolvableType.forClass(IRest.class).isAssignableFrom(interfaceType)) {
				ResolvableType[] genericTypes = interfaceType.getGenerics();
				for(ResolvableType genericType : genericTypes) {
					if(ResolvableType.forClass(JpaEntity.class).isAssignableFrom(genericType)) {
						entityResolvableType = genericType;
					}
				}
				break;
			}
		}
		Class<?> entityClass = entityResolvableType.getRawClass();
		return entityClass;
	}
	
	default Class<?> getIdClass() {
		ResolvableType thisResolvableType = ResolvableType.forClass(this.getClass());
		ResolvableType[] interfaceTypes = thisResolvableType.getInterfaces();
		ResolvableType entityResolvableType = null;
		for(ResolvableType interfaceType : interfaceTypes) {
			if(ResolvableType.forClass(IRest.class).isAssignableFrom(interfaceType)) {
				ResolvableType[] genericTypes = interfaceType.getGenerics();
				for(ResolvableType genericType : genericTypes) {
					if(ResolvableType.forClass(Serializable.class).isAssignableFrom(genericType)) {
						entityResolvableType = genericType;
					}
				}
				break;
			}
		}
		Class<?> entityClass = entityResolvableType.getRawClass();
		return entityClass;
	}
	
	default T generateEntity() throws InstantiationException, IllegalAccessException {
		Class<?> entityClass = this.getEntityClass();
		@SuppressWarnings("unchecked")
		T entity = (T) entityClass.newInstance();
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	default IRest<T, ID> getSelfComponent() {
		return (IRest<T, ID>) ApplicationContextHolder.getBean(this.getClass());
	}
	
	default IRepository<T, ID> getRepository() {
		ResolvableType repositoryType = ResolvableType.forClassWithGenerics(IRepository.class, this.getEntityClass(), this.getIdClass());
		String[] beanNames =  ApplicationContextHolder.getApplicationContext().getBeanNamesForType(repositoryType);
		for(String beanName : beanNames) {
			return ApplicationContextHolder.getBean(beanName);
		}
		return null;
	}
}
