package cn.edu.gznu.wecampus.core;

public class FrameworkConstant {
	public static String JWT_TOKEN_KEY = "jwtToken";
	
	public static int ERROR_CODE_LOGIN_FAIL = 1;
	public static int ERROR_CODE_LOGIN_STATUS_FAIL = 2;
	public static int ERROR_CODE_UNAUTHEN = 10;
	public static int ERROR_CODE_UNAUTHOR = 11;
	public static int ERROR_CODE_PARAMETER_VALID = 100;
	public static int ERROR_CODE_BUSINESS = 400;
	public static int ERROR_CODE_SYSTEM = 500;
}
