package cn.edu.gznu.wecampus.core.service;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

@Service
public class PropertyService {
	
	private Properties props = new Properties();
	
	@PostConstruct
	public void postConstruct() throws Exception {
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = resolver.getResources("classpath*:*.properties");
		for(Resource resource : resources) {
			props.load(resource.getInputStream());
			resource.getInputStream().close();
		}
	}
	
	public String getProperty(String key) {
		return props.getProperty(key);
	}
}
