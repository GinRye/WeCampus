package cn.edu.gznu.wecampus.core.common.repository.jpa;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.Role;

@Repository
public interface RoleRepository extends IRepository<Role, String> {
	
}
