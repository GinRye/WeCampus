package cn.edu.gznu.wecampus.core.weixin.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.edu.gznu.wecampus.core.weixin.entity.WeixinExpiresEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AccessTokenService {
	
	@Value("${weixin.corpid}")
	private String corpid;
	
	@Value("${weixin.corpsecret}")
	private String corpsecret;
	
	@Autowired
	private WeixinService<AccessTokenEntity> weixinService;
	
	@Getter
	@Setter
	public static class AccessTokenEntity extends WeixinExpiresEntity {
		@JsonProperty("access_token")
		private String accessToken;
	}

	@Getter
	@Setter
	private AccessTokenEntity entity;
	
	@PostConstruct
	public void postContruct() {
		this.generate();
	}
	
	public void generate() {
		this.entity = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("corpid", corpid);
		params.put("corpsecret", corpsecret);
		String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken";
		AccessTokenEntity token = weixinService.getForObject(url, AccessTokenEntity.class, params);
		this.entity = token;
		log.info("AccessToken: " + this.entity.getAccessToken());
	}
	
	@Scheduled(cron = "0 0/10 * * * ? ")
	public void schedule() {
		if(this.entity == null || this.entity.isExpire()) {
			this.generate();
		}
	}
}
