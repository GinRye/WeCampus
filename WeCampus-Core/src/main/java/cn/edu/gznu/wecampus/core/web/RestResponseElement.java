package cn.edu.gznu.wecampus.core.web;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponseElement extends RestResponse {
	private Object element;
}
