package cn.edu.gznu.wecampus.core.web.annotation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.springframework.stereotype.Component;

@Component
public class CriteriaEqualOperator extends AbstractCriteriaOperator {

	public CriteriaEqualOperator() {
		super("=");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Predicate createPredicate(CriteriaBuilder cb, From from, String field, Object value) {
		return cb.equal(from.get(field), value);
	}
}
