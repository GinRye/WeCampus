package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FormVo<ID extends Serializable> {
	private ID id;
	public ID getId() {
		return id;
	}
	public void setId(ID id) {
		this.id = id;
	}
}
