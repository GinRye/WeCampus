package cn.edu.gznu.wecampus.core;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IRepository<T extends JpaEntity<ID>, ID extends Serializable> 
	extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
	
	default T findOne(ID id) {
		if(id == null) {
			return null;
		}
		return this.findById(id).orElse(null);
	}
}
