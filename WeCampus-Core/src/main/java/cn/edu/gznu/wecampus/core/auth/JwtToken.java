package cn.edu.gznu.wecampus.core.auth;

import org.apache.shiro.authc.AuthenticationToken;

public class JwtToken implements AuthenticationToken {
	 
	private static final long serialVersionUID = 5129632309061089677L;
	
	private String token;
 
    public JwtToken(String token) {
        this.token = token;
    }
 
    @Override
    public Object getPrincipal() {
        return getToken();
    }
 
    @Override
    public Object getCredentials() {
        return getToken();
    }
    
    public String getToken() {
    	return token;
    }
}
