package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import cn.edu.gznu.wecampus.core.ApplicationContextHolder;
import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.service.PropertyService;
import cn.edu.gznu.wecampus.core.web.annotation.ApiPermissionConfig;

public interface IRemoveRest<T extends JpaEntity<ID>, ID extends Serializable> 
	extends IRest<T, ID> {

	@PostMapping(value = "/remove")
	@Transactional
	@ApiPermissionConfig("remove")
	default RestResponseElement remove(@RequestBody FormVo<ID> form) {
		T entity = getRepository().findOne(form.getId());
		if(entity == null) {
			PropertyService propertyService = ApplicationContextHolder.getBean(PropertyService.class);
			throw new RestException(
				propertyService.getProperty("pointcircle.framework.message.object-not-exist")
			);
		}
		IRemoveRest<T, ID> self = (IRemoveRest<T, ID>) this.getSelfComponent();
		self.processRemoveEntity(entity);
		return this.createRestResponseElement(entity);
	}
	
	void processRemoveEntity(T entity);
}
