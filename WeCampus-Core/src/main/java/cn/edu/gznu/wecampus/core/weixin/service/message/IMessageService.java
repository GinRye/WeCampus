package cn.edu.gznu.wecampus.core.weixin.service.message;

public interface IMessageService {
	
	boolean check(Request request);
	
	Response process(Request request);
}
