package cn.edu.gznu.wecampus.core.common.entity.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_common_operation_log")
@Getter
@Setter
public class OperationLog extends JpaEntity<String> {
	
	@Column(name = "log_time", nullable = false)
	private Date logTime;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "ip")
	private String ip = "";
	
	@Column(name = "content", length = 1024000)
	private String content = "";
}
