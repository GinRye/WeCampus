package cn.edu.gznu.wecampus.core.web.annotation;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.springframework.stereotype.Component;

@Component
public class CriteriaBetweenOperator extends AbstractCriteriaOperator {

	public CriteriaBetweenOperator() {
		super("between");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Predicate createPredicate(CriteriaBuilder cb, From from, String field, Object value) {
		List<Object> values = null;
		if(value instanceof List<?>) {
			values = (List<Object>) value;
		} else {
			values = new LinkedList<Object>();
			values.add(value);
		}
		return cb.between(from.get(field), (Comparable) values.get(0), (Comparable) values.get(1));
	}
}
