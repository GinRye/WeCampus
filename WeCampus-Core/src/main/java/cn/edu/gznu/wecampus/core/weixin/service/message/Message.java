package cn.edu.gznu.wecampus.core.weixin.service.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "xml")
public class Message {
	@JacksonXmlProperty(localName = "ToUserName")
	@JacksonXmlCData
	private String toUserName;
	@JacksonXmlProperty(localName = "FromUserName")
	@JacksonXmlCData
	private String fromUserName;
	@JacksonXmlProperty(localName = "CreateTime")
	private String createTime;
	@JacksonXmlProperty(localName = "MsgType")
	@JacksonXmlCData
	private String msgType;
	@JacksonXmlProperty(localName = "Content")
	@JacksonXmlCData
	private String content;
}