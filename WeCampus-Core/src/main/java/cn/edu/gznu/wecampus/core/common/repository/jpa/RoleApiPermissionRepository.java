package cn.edu.gznu.wecampus.core.common.repository.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.Role;
import cn.edu.gznu.wecampus.core.common.entity.jpa.RoleApiPermission;

@Repository
public interface RoleApiPermissionRepository extends IRepository<RoleApiPermission, String> {
	List<RoleApiPermission> findByRole(Role role);
}
