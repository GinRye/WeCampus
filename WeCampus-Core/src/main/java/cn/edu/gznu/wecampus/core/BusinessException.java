package cn.edu.gznu.wecampus.core;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 7306788296188614504L;

	public BusinessException() {
		super();
	}
	
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(Throwable e) {
		super(e);
	}
	
	public BusinessException(String message, Throwable e) {
		super(message, e);
	}
}
