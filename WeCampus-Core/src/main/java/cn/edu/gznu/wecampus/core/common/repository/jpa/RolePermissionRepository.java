package cn.edu.gznu.wecampus.core.common.repository.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.Role;
import cn.edu.gznu.wecampus.core.common.entity.jpa.RolePermission;

@Repository
public interface RolePermissionRepository extends IRepository<RolePermission, String> {
	List<RolePermission> findByRole(Role role);
}
