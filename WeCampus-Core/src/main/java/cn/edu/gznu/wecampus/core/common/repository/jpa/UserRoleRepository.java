package cn.edu.gznu.wecampus.core.common.repository.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.Role;
import cn.edu.gznu.wecampus.core.common.entity.jpa.User;
import cn.edu.gznu.wecampus.core.common.entity.jpa.UserRole;

@Repository
public interface UserRoleRepository extends IRepository<UserRole, String> {
	
	List<UserRole> findByUser(User user);
	
	List<UserRole> findByRole(Role role);
}
