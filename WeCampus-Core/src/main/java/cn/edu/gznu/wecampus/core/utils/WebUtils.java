package cn.edu.gznu.wecampus.core.utils;

import org.springframework.web.context.WebApplicationContext;

import cn.edu.gznu.wecampus.core.ApplicationContextHolder;

public class WebUtils {
	
	public static String getContextPath() {
		 WebApplicationContext webApplicationContext = 
			(WebApplicationContext) ApplicationContextHolder.getApplicationContext();
		 return webApplicationContext.getServletContext().getContextPath();
	}
}
