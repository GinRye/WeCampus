package cn.edu.gznu.wecampus.core.common.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_common_role_group")
@Getter
@Setter
public class RoleGroup extends JpaEntity<String> {
	
	@Column(name = "role_group_name", nullable = false)
	private String roleGroupName;
}
