package cn.edu.gznu.wecampus.core.web.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SearchParam {
	String[] fields();
	
	String operator() default "equal";
	
	boolean not() default false;
}
