package cn.edu.gznu.wecampus.core.weixin.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.edu.gznu.wecampus.core.weixin.entity.WeixinEntity;
import lombok.Getter;
import lombok.Setter;

@Service
public class UserInfoService {
	
	@Autowired
	private WeixinService<UserInfoEntity> weixinService;
	
	@Autowired
	private AccessTokenService accessTokenService;
	
	@Getter
	@Setter
	public static class UserInfoEntity extends WeixinEntity {
		@JsonProperty("UserId")
		private String userId;
		@JsonProperty("DeviceId")
		private String deviceId;
	}

	@Getter
	@Setter
	private UserInfoEntity userInfo;
	
	public UserInfoEntity get(String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("access_token", accessTokenService.getEntity().getAccessToken());
		params.put("code", code);
		String url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?";
		UserInfoEntity userInfo = weixinService.getForObject(url, UserInfoEntity.class, params);
		return userInfo;
	}
}
