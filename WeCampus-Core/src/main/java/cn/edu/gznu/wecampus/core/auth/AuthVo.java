package cn.edu.gznu.wecampus.core.auth;

import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthVo {
	private String xgh;
	private String name;
	private String jwtToken;
	private String weixinTicket;
	private List<String> permissionIds = new LinkedList<String>();
}
