package cn.edu.gznu.wecampus.core.weixin.service;

import cn.edu.gznu.wecampus.core.BusinessException;

public class WeixinException extends BusinessException {

	private static final long serialVersionUID = -2033266290209636125L;

	public WeixinException() {
		super();
	}
	
	public WeixinException(String message) {
		super(message);
	}
	
	public WeixinException(Throwable e) {
		super(e);
	}
	
	public WeixinException(String message, Throwable e) {
		super(message, e);
	}
}
