package cn.edu.gznu.wecampus.core.common.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.edu.gznu.wecampus.core.JpaEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ForeignKey;

@Entity
@Table(name = "tb_common_user_role")
@Getter
@Setter
public class UserRole extends JpaEntity<String> {
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_common_user_role_user_id"))
	private User user;

	@Column(name = "user_id", updatable = false, insertable = false)
	private String userId;

	@ManyToOne
	@JoinColumn(name = "role_id", nullable = false, 
		foreignKey = @ForeignKey(name = "fk_tb_common_user_role_role_id"))
	private Role role;

	@Column(name = "role_id", updatable = false, insertable = false)
	private String roleId;
}
