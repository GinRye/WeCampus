package cn.edu.gznu.wecampus.core.web;

import cn.edu.gznu.wecampus.core.BusinessException;

public class RestException extends BusinessException {

	private static final long serialVersionUID = -2033266290209636125L;

	public RestException() {
		super();
	}
	
	public RestException(String message) {
		super(message);
	}
	
	public RestException(Throwable e) {
		super(e);
	}
	
	public RestException(String message, Throwable e) {
		super(message, e);
	}

}
