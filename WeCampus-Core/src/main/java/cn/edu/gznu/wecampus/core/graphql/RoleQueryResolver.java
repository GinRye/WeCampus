package cn.edu.gznu.wecampus.core.graphql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import cn.edu.gznu.wecampus.core.common.entity.jpa.Role;
import cn.edu.gznu.wecampus.core.common.repository.jpa.RoleRepository;

@Component
public class RoleQueryResolver implements GraphQLQueryResolver {
	
	@Autowired
	private RoleRepository roleRepository;
	
    public List<Role> findAll() {
    	return roleRepository.findAll();
    }
}
