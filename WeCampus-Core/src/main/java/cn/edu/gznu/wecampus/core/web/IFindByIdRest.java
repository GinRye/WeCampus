package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.gznu.wecampus.core.JpaEntity;

public interface IFindByIdRest<T extends JpaEntity<ID>, ID extends Serializable> 
	extends IRest<T, ID> {

	@GetMapping("/findById")
	default RestResponseElement findById(@RequestParam("id") ID id) {
		T entity = getRepository().findById(id).orElse(null);
		return this.createRestResponseElement(entity);
	}
}
