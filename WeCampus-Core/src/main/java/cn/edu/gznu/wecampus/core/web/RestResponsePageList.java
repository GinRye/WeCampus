package cn.edu.gznu.wecampus.core.web;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponsePageList extends RestResponseList {
	private int page;
	private int size;
	private int totalPages;
	private int totalElements;
}
