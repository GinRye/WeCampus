package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.utils.PageableUtils;

public interface IFindPageRest<T extends JpaEntity<ID>, ID extends Serializable> 
	extends IRest<T, ID> {

	@GetMapping("/findPage")
	default RestResponseList findAllByPage(
		@RequestParam(value = "page", defaultValue = "1") int page, 
		@RequestParam(value = "size", defaultValue = "10") int size) {
		Pageable pageable = PageableUtils.of(page, size);
		Page<T> entities = getRepository().findAll(pageable);
		return this.createRestResponsePageList(pageable, entities);
	}
}
