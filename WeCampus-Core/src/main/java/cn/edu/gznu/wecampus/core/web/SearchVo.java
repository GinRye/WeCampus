package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;
import java.util.List;

import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.web.annotation.SearchParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SearchVo<T extends JpaEntity<ID>, ID extends Serializable> {
	private int page = 1;
	private int size = 10;
	@SearchParam(fields = "id", operator = "=")
	protected ID id;
	@SearchParam(fields = "id", operator = "in")
	protected List<ID> ids;
	@SearchParam(fields = "id", operator = "is null", not = true)
	protected int nnid = 1;
}
