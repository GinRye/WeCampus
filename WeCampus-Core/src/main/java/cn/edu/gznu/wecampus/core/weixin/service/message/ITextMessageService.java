package cn.edu.gznu.wecampus.core.weixin.service.message;

import org.apache.commons.lang3.StringUtils;

public interface ITextMessageService extends IMessageService {
	default boolean check(Request request) {
		if(!StringUtils.equals(request.getMsgType(), "text")) {
			return false;
		}
		return this.checkContent(request);
	}
	
	boolean checkContent(Request request);
}
