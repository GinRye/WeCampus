package cn.edu.gznu.wecampus.core.common.repository.jpa;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.User;

@Repository
public interface UserRepository extends IRepository<User, String> {
	
	User findByUsername(String username);
	
	@Query("select ur.roleId from UserRole ur where ur.userId = :id")
	List<String> findRolesById(@Param("id") String id);
	
	@Query(nativeQuery = true, value = ""
		+ "SELECT ap.permission "
		+ "FROM t_user_role ur "
		+ "LEFT JOIN t_role_api_permission rap "
		+ "ON ur.role_id = rap.role_id "
		+ "LEFT JOIN t_api_permission ap "
		+ "ON rap.api_permission_id = ap.api_permission_id "
		+ "WHERE ur.user_id = :id")
	Set<String> findApiPermissionsById(@Param("id") String id);
}
