package cn.edu.gznu.wecampus.core.common.repository.jpa;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.RoleGroup;

@Repository
public interface RoleGroupRepository extends IRepository<RoleGroup, String> {
	
}
