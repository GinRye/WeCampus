package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import org.springframework.core.ResolvableType;

import cn.edu.gznu.wecampus.core.JpaEntity;

public interface IFormRest <T extends JpaEntity<ID>, ID extends Serializable, FV extends FormVo<ID>> 
	extends IRest<T, ID> {

	@SuppressWarnings("unchecked")
	default Class<? extends FV> getFormVoClass() {
		ResolvableType thisResolvableType = ResolvableType.forClass(this.getClass());
		ResolvableType[] interfaceTypes = thisResolvableType.getInterfaces();
		ResolvableType entityVoResolvableType = null;
		for(ResolvableType interfaceType : interfaceTypes) {
			if(ResolvableType.forClass(IRest.class).isAssignableFrom(interfaceType)) {
				ResolvableType[] genericTypes = interfaceType.getGenerics();
				for(ResolvableType genericType : genericTypes) {
					if(ResolvableType.forClass(FormVo.class).isAssignableFrom(genericType)) {
						entityVoResolvableType = genericType;
					}
				}
				if(entityVoResolvableType != null) {
					break;
				}
			}
		}
		Class<? extends FV> entityVoClass = (Class<? extends FV>) entityVoResolvableType.getRawClass();
		return entityVoClass;
	}
}
