package cn.edu.gznu.wecampus.core.weixin.service.message;

import cn.edu.gznu.wecampus.core.BusinessException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageException extends BusinessException {

	private static final long serialVersionUID = -2033266290209636125L;
	
	private Request request;
	
	public MessageException() {
		super();
	}
	
	public MessageException(String message) {
		super(message);
	}
	
	public MessageException(Throwable e) {
		super(e);
	}
	
	public MessageException(String message, Throwable e) {
		super(message, e);
	}
}
