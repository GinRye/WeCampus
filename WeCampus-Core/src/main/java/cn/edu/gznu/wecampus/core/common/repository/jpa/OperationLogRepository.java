package cn.edu.gznu.wecampus.core.common.repository.jpa;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.OperationLog;

@Repository
public interface OperationLogRepository extends IRepository<OperationLog, String> {
	
}
