package cn.edu.gznu.wecampus.core.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PageableUtils {

	public static Pageable of(int page, int size) {
		return PageRequest.of(page - 1, size);
	}
}
