package cn.edu.gznu.wecampus.core.weixin.service.message;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageProcessor implements IMessageService {

	@Autowired(required = false)
	private List<IMessageService> services = new LinkedList<IMessageService>();

	@Override
	public boolean check(Request request) {
		return false;
	}

	@Override
	public Response process(Request request) {
		for(IMessageService service : services) {
			if(service.check(request)) {
				return service.process(request);
			}
		}
		if(StringUtils.equals(request.getMsgType(), "text")) {
			throw new MessageException("消息格式不正确");
		}
		return null;
	}
}
