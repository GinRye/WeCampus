package cn.edu.gznu.wecampus.core.common.repository.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.edu.gznu.wecampus.core.IRepository;
import cn.edu.gznu.wecampus.core.common.entity.jpa.ApiPermission;

@Repository
public interface ApiPermissionRepository extends IRepository<ApiPermission, String> {
	
	List<ApiPermission> findByApi(String api);

	List<ApiPermission> findByApiAndPermission(String api, String permission);
}
