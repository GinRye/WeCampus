package cn.edu.gznu.wecampus.core.web;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import cn.edu.gznu.wecampus.core.UserContext;
import cn.edu.gznu.wecampus.core.utils.AuthUtils;

@Component
public class WebUserContext implements UserContext {

	@Override
	public String getUserId() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if(requestAttributes == null) {
			return "AUTO";
		} else {
			if(AuthUtils.getUserId() == null) {
				return "UNAUTH_USER";
			} else {
				return AuthUtils.getUserId();
			}
		}
	}
}
