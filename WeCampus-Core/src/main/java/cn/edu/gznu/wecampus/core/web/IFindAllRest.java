package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;

import cn.edu.gznu.wecampus.core.JpaEntity;

public interface IFindAllRest<T extends JpaEntity<ID>, ID extends Serializable> 
	extends IRest<T, ID> {

	@GetMapping("/findAll")
	default RestResponseList findAll() {
		List<T> entities = getRepository().findAll();
		return this.createRestResponseList(entities);
	}

	
}
