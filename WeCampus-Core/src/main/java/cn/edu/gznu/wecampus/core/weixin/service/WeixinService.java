package cn.edu.gznu.wecampus.core.weixin.service;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cn.edu.gznu.wecampus.core.weixin.entity.WeixinEntity;

@Service
@Scope("prototype")
public class WeixinService<T extends WeixinEntity> {

	public T getForObject(String url, Class<T> responseType, Map<String, Object> uriVariables) {
		RestTemplate restTemplate = new RestTemplate();
		if(!url.endsWith("?")) {
			url += "?";
		}
		for(String key : uriVariables.keySet()) {
			url += key + "=" + "{" + key + "}&";
		}
		T entity = restTemplate.getForObject(url, responseType, uriVariables);
		if(entity.getErrcode() != 0) {
			throw new WeixinException(entity.getErrmsg());
		}
		return entity;
	}
}
