package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.web.annotation.ApiPermissionConfig;

@Validated
public interface IAddRest<T extends JpaEntity<ID>, ID extends Serializable, FV extends FormVo<ID>> 
	extends IFormRest<T, ID, FV> {

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/add")
	@ApiPermissionConfig("add")
	default RestResponseElement add(@RequestBody String json) throws Exception {
		try {
			T entity = this.generateEntity();
			ObjectMapper mapper = new ObjectMapper();
			FV form = mapper.readValue(json, this.getFormVoClass());
			IAddRest<T, ID, FV> self = (IAddRest<T, ID, FV>) this.getSelfComponent();
			self.processAddEntity(form, entity);
			return this.createRestResponseElement(entity);
		} catch(Exception e) {
			throw new RestException(e);
		}
	}
	
	void processAddEntity(@Valid FV form, T entity) throws Exception;
}
