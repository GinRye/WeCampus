package cn.edu.gznu.wecampus.core.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.gznu.wecampus.core.BusinessException;
import cn.edu.gznu.wecampus.core.web.annotation.AbstractCriteriaOperator;

@Service
public class CriteriaOperatorService {
	@Autowired
	private List<AbstractCriteriaOperator> operators;
	
	private Map<String, AbstractCriteriaOperator> operatorMap 
		= new HashMap<String, AbstractCriteriaOperator>();	
	
	@PostConstruct
	public void postConstruct() {
		for(AbstractCriteriaOperator operator : operators) {
			String name = operator.getName();
			AbstractCriteriaOperator existOperator = operatorMap.get(name);
			if(existOperator != null) {
				throw new BusinessException(
					"AbstractCriteriaOperator has same name! " +
					operator.getClass().getName() + ", " + 
					existOperator.getClass().getName()
				);
			}
			operatorMap.put(name, operator);
		}
	}
	
	public AbstractCriteriaOperator getOperator(String name) {
		return operatorMap.get(name);
	}
}
