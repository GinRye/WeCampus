package cn.edu.gznu.wecampus.core.web;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.gznu.wecampus.core.JpaEntity;

public interface IFindByIdsRest<T extends JpaEntity<ID>, ID extends Serializable> 
	extends IRest<T, ID> {

	@GetMapping("/findByIds")
	default RestResponseList findByIds(@RequestParam("ids") List<ID> ids) {
		List<T> entities = getRepository().findAllById(ids);
		RestResponseList response = new RestResponseList();
		response.setSuccess(true);
		response.setList(entities);
		return response;
	}
}
