package cn.edu.gznu.wecampus.core.web.annotation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.springframework.stereotype.Component;

@Component
public class CriteriaLikeOperator extends AbstractCriteriaOperator {

	public CriteriaLikeOperator() {
		super("like");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Predicate createPredicate(CriteriaBuilder cb, From from, String field, Object value) {
		Expression<?> exp = from.get(field);
		return cb.like((Expression<String>) exp, "%" + value + "%");
	}
}
