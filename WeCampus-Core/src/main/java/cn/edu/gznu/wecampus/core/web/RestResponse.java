package cn.edu.gznu.wecampus.core.web;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponse {
	private boolean success = true;
	private Detail detail = new Detail();
	
	@Getter
	@Setter
	public static class Detail {
		public int code = 0;
		public String msg = "";
	}
}
