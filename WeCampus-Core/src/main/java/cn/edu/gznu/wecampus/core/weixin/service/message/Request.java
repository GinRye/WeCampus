package cn.edu.gznu.wecampus.core.weixin.service.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "xml")
public class Request extends Message {
	@JacksonXmlProperty(localName = "MsgId")
	@JacksonXmlCData
	private String msgId;
	@JacksonXmlProperty(localName = "AgentID")
	@JacksonXmlCData
	private String agentID;
}
