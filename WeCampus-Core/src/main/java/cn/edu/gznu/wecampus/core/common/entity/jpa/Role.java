package cn.edu.gznu.wecampus.core.common.entity.jpa;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.edu.gznu.wecampus.core.ApplicationContextHolder;
import cn.edu.gznu.wecampus.core.JpaEntity;
import cn.edu.gznu.wecampus.core.common.repository.jpa.RolePermissionRepository;
import cn.edu.gznu.wecampus.core.common.repository.jpa.UserRoleRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_common_role")
public class Role extends JpaEntity<String> {
	
	@Column(name = "role_name", nullable = false)
	private String roleName;
	
	@JsonProperty("permissionIds")
	public List<String> permissionIds() {
		RolePermissionRepository rolePermissionRepository = ApplicationContextHolder.getBean(RolePermissionRepository.class);
		return rolePermissionRepository.findByRole(this).stream()
			.map(rolePermission -> rolePermission.getPermission())
			.collect(Collectors.toList());
	}
	
	@JsonProperty("userIds")
	public List<String> userIds() {
		UserRoleRepository userRoleRepository = ApplicationContextHolder.getBean(UserRoleRepository.class);
		return userRoleRepository.findByRole(this).stream()
			.map(userRole -> userRole.getUserId())
			.collect(Collectors.toList());
	}
}
