package cn.edu.gznu.wecampus.core;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class JpaEntity<ID extends Serializable> {

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	@GeneratedValue(generator = "system-uuid")
	@Column(name = "id")
	private String id;
	
	@Column(name = "create_time")
	protected Date createTime;

	@Column(name = "create_user")
	protected String createUser;

	@Column(name = "update_time")
	protected Date updateTime;

	@Column(name = "update_user")
	protected String updateUser;
	
	@JsonIgnore
	public boolean isNew() {
		return this.getId() == null;
	}
}
