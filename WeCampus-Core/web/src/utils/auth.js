import {getStore, setStore, clearStore} from '@/utils/storage'

export const userKey = 'user';
export const orgKey = 'org';
export const projectIdKey = 'projectId';

export function getUser() {
	var user = getStore(userKey);
	return user;
}

export function setUser(user) {
	return setStore(userKey, user)
}

export function clearUser() {
	return clearStore(userKey)
}

export function getOrg() {
	var org = getStore(orgKey);
	return org;
}

export function setOrg(org) {
	return setStore(orgKey, org)
}

export function clearOrg() {
	return clearStore(orgKey)
}
