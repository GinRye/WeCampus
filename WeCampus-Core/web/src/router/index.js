import auth from '@/views/auth.vue';

export default [{
	path: '/auth',
	component: auth,
	name: 'auth'
}];
